package de.rwth.swc.teaching.sqa.vendingmachine;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class WorstCaseTestGenerator {

    private static List<List<Integer>> cartesianProduct(List<List<Integer>> lists) {
        List<List<Integer>> resultProduct = new ArrayList<>();
        if (lists.size() == 0) {
            resultProduct.add(new ArrayList<>());
            return resultProduct;
        } else {
            List<Integer> firstList = lists.get(0);
            List<List<Integer>> remainingLists = cartesianProduct(lists.subList(1, lists.size()));
            for (Integer itemList : firstList) {
                for (List<Integer> remainingList : remainingLists) {
                    ArrayList<Integer> resultList = new ArrayList<>();
                    resultList.add(itemList);
                    resultList.addAll(remainingList);
                    resultProduct.add(resultList);
                }
            }
        }
        return resultProduct;
    }

    public static void generateTestCaseFile(List<List<Integer>> lists, String fileName) {
        List<List<Integer>> product = cartesianProduct(lists);
        try (PrintWriter writer = new PrintWriter(new File(fileName))) {
            String sb = "coke" +
                    ',' +
                    "lemonade" +
                    ',' +
                    "water" +
                    '\n';
            writer.write(sb);
            for(List<Integer> row : product) {
                String outputRow = String.valueOf(row.get(0)) +
                        ',' +
                        row.get(1) +
                        ',' +
                        row.get(2) +
                        '\n';
                writer.write(outputRow);
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }

}
