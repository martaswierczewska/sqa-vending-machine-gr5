package de.rwth.swc.teaching.sqa.vendingmachine;

import de.rwth.swc.teaching.sqa.Drink;
import de.rwth.swc.teaching.sqa.VendingMachine;
import de.rwth.swc.teaching.sqa.exception.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.util.Arrays;
import java.util.List;

public class VendingMachineNPlusBVTest {

    private static VendingMachine machine;

    @BeforeAll
    public static void init(){
        List<Integer> worstRobustBVCokeLengths = Arrays.asList(-1,0,1,5,9,10,11);
        List<Integer> worstRobustBVDLemonadeLengths = Arrays.asList(-1,0,1,5,9,10,11);
        List<Integer> worstRobustCaseWaterLengths = Arrays.asList(-1,0,1,5,9,10,11);
        List<List<Integer>> inputSetsWorstRobust = Arrays.asList(worstRobustBVCokeLengths,
                worstRobustBVDLemonadeLengths,
                worstRobustCaseWaterLengths);
        WorstCaseTestGenerator.generateTestCaseFile(inputSetsWorstRobust,
                "src/test/java/worst_case_robustness_tests.csv");

    }

    @ParameterizedTest
    @CsvFileSource(files = "src/test/java/worst_case_robustness_tests.csv", numLinesToSkip = 1)
    public void testGetOneCoke(int coke, int lemonade, int water) throws InvalidDrinkException, CoinInsertedException, MachineEmptyException, NoCoinInsertedException, InvalidDrinkNumberException {
        machine = new VendingMachine(coke, lemonade, water);
        machine.insertCoin();
        Assertions.assertTrue(machine.coinInserted());
        machine.getDrink(Drink.COKE);
        Assertions.assertTrue(machine.drinkEmpty(Drink.COKE));
    }

    @ParameterizedTest
    @CsvFileSource(files = "src/test/java/worst_case_robustness_tests.csv", numLinesToSkip = 1)
    public void testGetOneCokeWithoutCoin(int coke, int lemonade, int water) throws InvalidDrinkNumberException {
        machine = new VendingMachine(coke, lemonade, water);
        Assertions.assertFalse(machine.coinInserted());
        Executable executable = () -> machine.getDrink(Drink.COKE);
        Assertions.assertThrows(NoCoinInsertedException.class, executable);
    }

    @ParameterizedTest
    @CsvFileSource(files = "src/test/java/worst_case_robustness_tests.csv", numLinesToSkip = 1)
    public void testGetOneCokeWithoutDrinks(int coke, int lemonade, int water) throws InvalidDrinkNumberException, InvalidDrinkException, CoinInsertedException {
        machine = new VendingMachine(coke, lemonade, water);
        machine.insertCoin();

        Assertions.assertTrue(machine.drinkEmpty(Drink.COKE));
        Executable executable = () -> machine.getDrink(Drink.COKE);
        Assertions.assertThrows(MachineEmptyException.class, executable);
    }

    @ParameterizedTest
    @CsvFileSource(files = "src/test/java/worst_case_robustness_tests.csv", numLinesToSkip = 1)
    public void testFillDrinkWhenDrinksFull(int coke, int lemonade, int water) throws InvalidDrinkException, MachineFullException, InvalidDrinkNumberException {
        machine = new VendingMachine(coke, lemonade, water);
        while (!machine.drinkFull(Drink.COKE)) {
            machine.fillWithDrink(Drink.COKE);
        }

        Assertions.assertTrue(machine.drinkFull(Drink.COKE));
        Executable executable = () -> machine.fillWithDrink(Drink.COKE);
        Assertions.assertThrows(MachineFullException.class, executable);
    }

    @ParameterizedTest
    @CsvFileSource(files = "src/test/java/worst_case_robustness_tests.csv", numLinesToSkip = 1)
    public void testInsertCoinWhenCoinInserted(int coke, int lemonade, int water) throws CoinInsertedException, InvalidDrinkNumberException {
        machine = new VendingMachine(coke, lemonade, water);
        machine.insertCoin();
        Assertions.assertTrue(machine.coinInserted());
        Executable executable = () -> machine.insertCoin();
        Assertions.assertThrows(CoinInsertedException.class, executable);
    }

    @ParameterizedTest
    @CsvFileSource(files = "src/test/java/worst_case_robustness_tests.csv", numLinesToSkip = 1)
    public void testMoneyBackWhenCoinInserted(int coke, int lemonade, int water) throws InvalidDrinkNumberException {
        machine = new VendingMachine(coke, lemonade, water);
        Assertions.assertFalse(machine.coinInserted());
        Executable executable = () -> machine.moneyBack();
        Assertions.assertThrows(NoCoinInsertedException.class, executable);
    }



}
