package de.rwth.swc.teaching.sqa.vendingmachine;

import de.rwth.swc.teaching.sqa.Drink;
import de.rwth.swc.teaching.sqa.VendingMachine;
import de.rwth.swc.teaching.sqa.exception.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

public class VendingMachineTest {

    private static VendingMachine machine;

    @BeforeEach
    void init() throws InvalidDrinkNumberException {
        machine = new VendingMachine(1, 0, 0);
    }

    @Test
    public void testGetOneCoke() throws InvalidDrinkException, CoinInsertedException, MachineEmptyException, NoCoinInsertedException {
        machine.insertCoin();
        Assertions.assertTrue(machine.coinInserted());
        machine.getDrink(Drink.COKE);
        Assertions.assertTrue(machine.drinkEmpty(Drink.COKE));
    }

    @Test
    public void testGetOneCokeWithoutCoin() {
        Assertions.assertFalse(machine.coinInserted());
        Executable executable = () -> machine.getDrink(Drink.COKE);
        Assertions.assertThrows(NoCoinInsertedException.class, executable);
    }

    @Test
    public void testGetOneCokeWithoutDrinks() throws InvalidDrinkNumberException, InvalidDrinkException, CoinInsertedException {
        machine = new VendingMachine(0, 0, 0);
        machine.insertCoin();

        Assertions.assertTrue(machine.drinkEmpty(Drink.COKE));
        Executable executable = () -> machine.getDrink(Drink.COKE);
        Assertions.assertThrows(MachineEmptyException.class, executable);
    }

    @Test
    public void testFillDrinkWhenDrinksFull() throws InvalidDrinkException, MachineFullException {
        while (!machine.drinkFull(Drink.COKE)) {
            machine.fillWithDrink(Drink.COKE);
        }

        Assertions.assertTrue(machine.drinkFull(Drink.COKE));
        Executable executable = () -> machine.fillWithDrink(Drink.COKE);
        Assertions.assertThrows(MachineFullException.class, executable);
    }

    @Test
    public void testInsertCoinWhenCoinInserted() throws CoinInsertedException {
        machine.insertCoin();
        Assertions.assertTrue(machine.coinInserted());
        Executable executable = () -> machine.insertCoin();
        Assertions.assertThrows(CoinInsertedException.class, executable);
    }

    @Test
    public void testMoneyBackWhenCoinInserted() {
        Assertions.assertFalse(machine.coinInserted());
        Executable executable = () -> machine.moneyBack();
        Assertions.assertThrows(NoCoinInsertedException.class, executable);
    }
}
